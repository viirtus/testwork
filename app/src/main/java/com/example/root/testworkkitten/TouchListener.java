package com.example.root.testworkkitten;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;

/**
 * Created by root on 17.03.15.
 */
public class TouchListener implements View.OnTouchListener {
    private LinearLayout footer;
    private int deviceHeight;
    private float lastTouchY = 0;
    private boolean lock = false;
    private boolean outLock = false;
    private boolean inLock = false;
    private boolean isTouch = false;
    private boolean isUpScrollDirection = false;
    private float alpha = 1;
    public TouchListener (Context context, LinearLayout footer) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        this.deviceHeight = displayMetrics.heightPixels;
        this.footer = footer;
    }
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        int action = event.getAction();
        switch (action) {
            case MotionEvent.ACTION_MOVE:
                float y = event.getY();
                float delta = y - lastTouchY;

                //Used for prevent wrong direction detect
                if (!lock) {
                    invalidateFooter(delta);
                } else {
                    lock = false;
                }
                break;
            case MotionEvent.ACTION_DOWN:
                lastTouchY = event.getY();
                lock = true;
                isTouch = true;
                break;
            case MotionEvent.ACTION_UP:
                isTouch = false;
                if (isUpScrollDirection) {
                    alpha = 1;
                } else {
                    alpha = 0;
                }
                footer.setAlpha(alpha);
                break;
        }
        return false;
    }

    /**
     * Method set alpha to footer. Alpha calculated as a ratio offset and device height.
     * alpha = 1 when we scrolled 25% of screen height. Using locks for prevent alpha blink
     * when we again scroll the same direction. Also, detect scroll direction;
     * @param offset of touch-scrolling in pixels
     */
    private void invalidateFooter (float offset) {
        if (offset > 0 && inLock) {
            return;
        }
        if (offset <= 0 && outLock) {
            return;
        }
        inLock = false;
        outLock = false;
        float normal = offset / deviceHeight;
        if (offset > 0) {
            isUpScrollDirection = true;
            alpha = normal;
        } else {
            isUpScrollDirection = false;
            alpha = 0.25f + normal;
        }
        alpha *= 4;
        if (alpha < 0) outLock = true;
        if (alpha >= 1) inLock = true;

        if ((!outLock || !inLock) && isTouch) footer.setAlpha(alpha);
    }
}
