package com.example.root.testworkkitten.store;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Class is used for storing and getting already loaded images in/from local storage
 * Created by root on 17.03.15.
 */
public class ImageStore {
    private String fullPath;

    public ImageStore(Context context) throws IOException {
        this.fullPath = context.getFilesDir().getAbsolutePath() + File.separator + "images";

        //create a sub dir for our images
        File storedDir = new File(this.fullPath);
        if (!storedDir.exists()) {
            boolean success = storedDir.mkdir();
            if (!success) throw new IOException("Cannot create internal storage");
        }
    }

    /**
     * write new image into storage
     * @param image just now downloaded
     * @param position in a list view
     * @throws IOException
     */
    public void storeNew(Bitmap image, int position) throws IOException {
        File imgFile = new File(fullPath + File.separator + position);
        imgFile.createNewFile();
        FileOutputStream out = new FileOutputStream(imgFile);
        image.compress(Bitmap.CompressFormat.PNG, 100, out);
        out.close();
    }

    /**
     * getting image for required position
     * @param position in a list view
     * @return bitmap if exist
     * @throws IOException if not exist
     */
    public Bitmap getImage(int position) throws IOException {
        File imgFile = new File(fullPath + File.separator + position);

        FileInputStream in = new FileInputStream(imgFile);

        Bitmap img = BitmapFactory.decodeStream(in);

        in.close();

        return img;
    }

    /**
     * destroy all stored images
     */
    public void destroy() {
        File dir = new File(fullPath );
        File[] files = dir.listFiles();
        for (File f : files) {
            f.delete();
        }
    }
}
