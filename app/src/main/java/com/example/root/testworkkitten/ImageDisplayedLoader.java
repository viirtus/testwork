package com.example.root.testworkkitten;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.View;
import android.widget.ImageView;

import com.example.root.testworkkitten.store.ImageStore;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.io.IOException;

/**
 * Created by root on 17.03.15.
 */
public class ImageDisplayedLoader {
    private ImageStore imageStore;
    /**
     * Handler which used for update ImageView state in UI thread
     */
    private static Handler handler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message m) {
            ImageStructure structure = (ImageStructure) m.obj;
            structure.view.setImageBitmap(structure.bitmap);
        }
    };
    public ImageDisplayedLoader(ImageStore imageStore) {
        this.imageStore = imageStore;
    }

    /**
     * Run file-loading in a new thread. Better use ThreadPool instead of this, but not now.
     * @param imageView reference to image view holder
     * @param position of list item
     */
    public void load (final ImageView imageView, final int position) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Bitmap image = imageStore.getImage(position);
                    ImageStructure structure = new ImageStructure(imageView, image);
                    Message m = handler.obtainMessage(0, structure);
                    handler.sendMessage(m);

                } catch (IOException e) {
                    ImageLoader.getInstance().displayImage(generateLink(), imageView, new LoaderListener(position));
                }
            }
        }).start();

    }
    private String generateLink () {
        int height = (int) ((Math.random() * 924 + 100));
        int width = (int) ((Math.random() * 924 + 100));
        return "http://placekitten.com/" + width + "/" + height;
    }

    /**
     * Custom load status listener which used for storing just now downloaded image
     */
    private class LoaderListener implements ImageLoadingListener {
        private int position;

        /**
         *
         * @param position is required for success local storing
         */
        public LoaderListener (int position) {
            this.position = position;
        }

        @Override
        public void onLoadingStarted(String imageUri, View view) {

        }

        @Override
        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

        }

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            ImageStructure structure = new ImageStructure(view, loadedImage);
            Message m = handler.obtainMessage(0, structure);
            handler.sendMessage(m);
            try {
                imageStore.storeNew(loadedImage, position);
            } catch (IOException ignore) {}
        }

        @Override
        public void onLoadingCancelled(String imageUri, View view) {

        }
    }

    /**
     * Simple class for sending data through threads
     */
    private class ImageStructure {
        private ImageView view;
        private Bitmap bitmap;
        public ImageStructure (View view, Bitmap bitmap) {
            this.view = (ImageView) view;
            this.bitmap = bitmap;
        }
    }
}
