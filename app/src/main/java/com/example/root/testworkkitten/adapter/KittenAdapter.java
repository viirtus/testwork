package com.example.root.testworkkitten.adapter;

import android.content.Context;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.etsy.android.grid.util.DynamicHeightImageView;
import com.example.root.testworkkitten.ImageDisplayedLoader;
import com.example.root.testworkkitten.R;
import java.util.Random;

public class KittenAdapter extends ArrayAdapter<String> {
    private static SparseArray<Double> positionHeightRatios = new SparseArray<>();
    private final LayoutInflater inflater;
    private final Random mRandom;
    private ImageDisplayedLoader loader;

    public KittenAdapter(Context context, int res, ImageDisplayedLoader loader) {
        super(context, res);
        this.inflater = LayoutInflater.from(context);
        this.mRandom = new Random();
        this.loader = loader;
    }

    /**
     *
     * @return count of pictures
     */
    @Override
    public int getCount() {
        return 50;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //standard viewholder pattern
        ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_layout, null);
            holder = new ViewHolder();
            holder.imageView = (DynamicHeightImageView) convertView.findViewById(R.id.kittenImage);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
            holder.imageView.setImageDrawable(null);
        }
        double positionHeight = getPositionRatio(position);
        holder.imageView.setHeightRatio(positionHeight);

        //loading pictures into the imageView via custom loader
        loader.load(holder.imageView, position);
        return convertView;
    }

    private double getPositionRatio(final int position) {
        double ratio = positionHeightRatios.get(position, 0.0);
        // if not yet done generate and stash the columns height
        // in our real world scenario this will be determined by
        // some match based on the known height and width of the image
        // and maybe a helpful way to get the column height!
        if (ratio == 0) {
            ratio = getRandomHeightRatio();
            positionHeightRatios.append(position, ratio);
        }
        return ratio;
    }

    private double getRandomHeightRatio() {
        return (mRandom.nextDouble() / 2.0) + 1.0; // height will be 1.0 - 1.5
    }

    class ViewHolder {
        DynamicHeightImageView imageView;
    }
}