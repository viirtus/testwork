package com.example.root.testworkkitten;

import java.io.IOException;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.etsy.android.grid.StaggeredGridView;
import com.example.root.testworkkitten.adapter.KittenAdapter;
import com.example.root.testworkkitten.store.ImageStore;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class MainActivity extends Activity {
    private StaggeredGridView gridView;
    private KittenAdapter kittenAdapter;
    private LinearLayout footer;
    private Button button;
    private ImageStore imageStore;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(this));


        try {
            imageStore = new ImageStore(this);
        } catch (IOException e) {
            return;
        }
        ImageDisplayedLoader loader = new ImageDisplayedLoader(imageStore);

        gridView = (StaggeredGridView) findViewById(R.id.grid_view);
        footer = (LinearLayout) findViewById(R.id.footer);
        button = (Button) findViewById(R.id.button);
        kittenAdapter = new KittenAdapter(this, android.R.layout.simple_list_item_1, loader);

    }

    @Override
    public void onResume() {
        super.onResume();
        gridView.setAdapter(kittenAdapter);
        gridView.setOnTouchListener(new TouchListener(this, footer));
        button.setOnClickListener(new View.OnClickListener() {
            /**
             * destroy all stored images and reload adapter
             * @param v
             */
            @Override
            public void onClick(View v) {
                imageStore.destroy();
                kittenAdapter.notifyDataSetChanged();
            }
        });
    }
    @Override
    public void onPause() {
        super.onPause();
        gridView.setAdapter(null);
        gridView.setOnTouchListener(null);
        button.setOnClickListener(null);
    }
}